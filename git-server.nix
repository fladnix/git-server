{ lib, config, pkgs, ... }:
with lib;
let cfg = config.gitServer;
in {
  options.gitServer = {
    enable = mkEnableOption "git server";
    authorizedKeys = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = "List of keys allowed to connect via ssh to git user";
    };
  };
  config = mkIf cfg.enable {
    users.users.git = {
      isSystemUser = true;
      description = "git user for ssh access to git repositories";
      home = "/srv/git";
      createHome = true;
      shell = "${pkgs.git}/bin/git-shell";
      group = "git";
      openssh.authorizedKeys.keys = cfg.authorizedKeys;
    };
    environment.systemPackages = with pkgs; [ git ];
    services.openssh.enable = true;
  };
}
