{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";

  outputs = { self, nixpkgs, ... }: {
    nixosModules = { gitServer = import ./git-server.nix; };
    nixosModule = self.nixosModules.gitServer;
  };
}
